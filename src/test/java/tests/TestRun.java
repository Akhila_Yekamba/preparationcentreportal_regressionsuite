package tests;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src\\test\\resources\\feature",
		glue = {"stepdefinition"},
		//tags = {"@validLogin"},
		//tags = {"@invalidLogin"},
//tags = {"@prepCentrePortal"},
		//tags = {"@StatmentofResult"},
		//tags = {"@ResultSummary"},
			//tags = {"@ResultSummaryPDFGenerate"},
			//tags =	{"@ResultSummaryExcelGenerate"},
		 //tags = {" @UserAdd"},
				//tags = {" @Help"},
		//tags = {"@validLogin", "@results"},
		plugin = {"com.cucumber.listener.ExtentCucumberFormatter:"},
		monochrome = true
		)

public class TestRun{

		@BeforeClass
			public static void setup() {
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss");
				final LocalDateTime now = LocalDateTime.now();
				String st = dtf.format(now);
				ExtentProperties extentProperties = ExtentProperties.INSTANCE;
				extentProperties.setReportPath("target\\Automation Test Report- 06-04-2023\\Report " + st + ".html");
			}  
		
		@AfterClass
		public static void teardown() {
			String Path1 = System.getProperty("user.dir") + "\\Resources\\extent-config.xml";
			Reporter.loadXMLConfig(new File(Path1));
			Reporter.setSystemInfo("user", System.getProperty("user.name"));
			Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
			Reporter.setSystemInfo("Execution Environment", "QA2 Environment");
			Reporter.setSystemInfo("Operating System", "Windows 10 - " + "64 Bit");
    		Reporter.setSystemInfo("Browser", "Chrome - " + "85.0.41");
			Reporter.setSystemInfo("Browser", "Firefox - " + "72.0.2");
			Reporter.setSystemInfo("Selenium", "3.4.0");
			Reporter.setSystemInfo("Java Version", "1.8.0_181");
			Reporter.setTestRunnerOutput("Preparation Centre Portal Test Suite Output");
		}
		
}