package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ResultsSummaryPDF extends CucumberBase{
	@Given("^User will navigate to Preparation Centre Portal application$")
	public void user_will_navigate_to_Preparation_Centre_Portal_application() throws Throwable {
		launchBrowser();
	}
	@When("^User enter the valid credentials$")
	public void user_enter_the_valid_credentials() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		enterUserNamePassword();
	}
	@Then("^User should be login$")
	public void user_should_be_login() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
	}

	

	@When("^User select Exam from the dropdown$")
	public void user_select_Exam_from_the_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		prepCentreResultsView();
	}

	@When("^User select session from the dropdown$")
	public void user_select_session_from_the_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}
	@When("^click on the Go button$")
	public void click_on_the_Go_button() throws Throwable {
	}
	
	
	@Then("^user is able to view the result$")
	public void user_is_able_to_view_the_result() throws Throwable {
	}
	
	@When("^user selects view as pdf radio button$")
	public void user_select_view_as_pdf_radio_button() throws Throwable{
		ResultSummaryPDFGenerate();
	}
	@When("^user clicks on Run Report$")
	public void user_clicks_on_Run_Report() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
	}//
	@Then("^user should be able to view the result in downloaded pdf file$")
	public void user_should_be_able_to_view_the_result_in_downloaded_pdf_file() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}
	
	  
	}

	
