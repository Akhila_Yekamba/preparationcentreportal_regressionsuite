package stepdefinition;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeOptions;


public class CucumberBase {

	public static WebDriver driver;
	public static final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	static Properties prop;
	static FileInputStream fis;
	
	public static void launchBrowser() throws Exception 
	{
		try {
			fis = new FileInputStream(System.getProperty("user.dir")+"\\config.properties"); 
//			fis = new FileInputStream(System.getProperty("user.home")+"\\Evo_Automation_Package\\config1.properties");
			prop = new Properties();
			prop.load(fis);
		} catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
		}
		
		if(prop.getProperty("browser").equalsIgnoreCase("firefox"))
		{

			System.setProperty("webdriver.gecko.driver", prop.getProperty("driverPath")+"geckodriver.exe");
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", true);

			driver = new FirefoxDriver();
			driver.get(prop.getProperty("url"));
			Thread.sleep(2000);
			driver.manage().window().maximize();


		}
		else if(prop.getProperty("browser").equalsIgnoreCase("chrome"))
		{
			ChromeOptions option = new ChromeOptions();
			option.addArguments("--remote-allow-origins=*");
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver(option);
			driver.manage().window().maximize();
			driver.get(prop.getProperty("url"));
//			return new ChromeDriver();
				
		}
		else if(prop.getProperty("browser").equalsIgnoreCase("IE"))
		{
			System.setProperty("webdriver.ie.driver",prop.getProperty("driverPath")+"IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("url"));
			driver.manage().window().maximize();
//			waitForPageLoaded();
//			return new InternetExplorerDriver();
				
		}
		else if(prop.getProperty("browser").equalsIgnoreCase("Edge"))
		{
			System.setProperty("webdriver.edge.driver",prop.getProperty("driverPath")+"MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
			driver.get(prop.getProperty("url"));
			driver.manage().window().maximize();
			
		}
		
		else
		{
			Assert.fail("Please choose a supported browser");
		}
	}
	
	
	public void enterUserNamePassword(){
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
		
		WebDriverWait waitForUsername = new WebDriverWait(driver, 30);
		waitForUsername.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ctl00_PCPortalMain_userNameText']")));
		
		WebDriverWait waitForPassword = new WebDriverWait(driver, 30);
		waitForPassword.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ctl00_PCPortalMain_passwordText']")));
		
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_userNameText']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_userNameText']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_userNameText']")).sendKeys(prop.getProperty("username")) ;

		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_passwordText']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_passwordText']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_passwordText']")).sendKeys(prop.getProperty("password")) ;

		WebElement logIn = driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_logOnButton']"));
		logIn.click();
		
	}
	
	public void enterinvalidUserNamePassword(){
		
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
		
		WebDriverWait waitForUsername = new WebDriverWait(driver, 30);
		waitForUsername.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ctl00_PCPortalMain_userNameText']")));
		
		WebDriverWait waitForPassword = new WebDriverWait(driver, 30);
		waitForPassword.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ctl00_PCPortalMain_passwordText']")));
		
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_userNameText']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_userNameText']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_userNameText']")).sendKeys(prop.getProperty("invalidusername")) ;

		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_passwordText']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_passwordText']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_passwordText']")).sendKeys(prop.getProperty("password")) ;

		WebElement logIn = driver.findElement(By.xpath("//*[@id='ctl00_PCPortalMain_logOnButton']"));
		logIn.click();
		
	}
      public void prepCentreResultsView() throws InterruptedException
   {
	   driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	   WebElement exam = driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_examDropDownList\"]"));
	   Thread.sleep(3000);
	   Select select= new Select(exam);
	   select.selectByIndex(1);
	   Thread.sleep(3000);
	   WebElement Session=driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_sessionDropDownList\"]"));
	   Select select1= new Select(Session);
	   select1.selectByIndex(1);
	   driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_searchButton\"]")).click();
	  
   }
   public void StatementofResult() throws InterruptedException
   {
	   Thread.sleep(3000);
	   JavascriptExecutor js = (JavascriptExecutor) driver;
	   js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	   driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_detailedCandidateResult\"]")).click();
	   driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_resultsSummaryBlock\"]/div[2]/fieldset/div")).click();
	
   }
   public void ResultSummary() throws InterruptedException
   {
      
   Thread.sleep(3000);
   JavascriptExecutor js = (JavascriptExecutor) driver;
   js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
   
       driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_runReportButton\"]")).click();
}
   public void UserAdd() throws InterruptedException {
   driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
   WebElement MyProfile = driver.findElement(By.xpath("//*[@id=\"nav\"]/li[4]/a/span"));
   Actions action = new Actions(driver);
   action.moveToElement(MyProfile).build().perform();
   driver.findElement(By.xpath("//*[@id=\"ctl00_additionalUsers\"]")).click();
   
  
   WebDriverWait waitForfirstname = new WebDriverWait(driver, 30);
   waitForfirstname.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ctl00_PCPortalMain_firstNameText\"]")));
	
	driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_firstNameText\"]")).click();
	driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_firstNameText\"]")).clear();
	driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_firstNameText\"]")).sendKeys(prop.getProperty("firstname")) ;
   
	WebDriverWait waitForlastname = new WebDriverWait(driver, 30);
	   waitForlastname.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ctl00_PCPortalMain_lastNameText\"]")));
		
		driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_lastNameText\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_lastNameText\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_lastNameText\"]")).sendKeys(prop.getProperty("lastname")) ;
	   
		WebDriverWait waitForemail = new WebDriverWait(driver, 30);
		   waitForemail.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ctl00_PCPortalMain_emailText\"]")));
			
			driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_emailText\"]")).click();
			driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_emailText\"]")).clear();
			driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_emailText\"]")).sendKeys(prop.getProperty("email")) ;
	
			WebElement addadditionaluser = driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_submitButton\"]"));
			addadditionaluser.click();
		   
   }
   public void ResultSummaryPDFGenerate() throws InterruptedException
   {
      
   Thread.sleep(3000);
   JavascriptExecutor js = (JavascriptExecutor) driver;
   js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
   driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_viewAsPDFRadioButton\"]")).click();
   
   
       driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_runReportButton\"]")).click();
}
   
   public void ResultSummaryExcelGenerate() throws InterruptedException
   {
      
   Thread.sleep(3000);
   JavascriptExecutor js = (JavascriptExecutor) driver;
   js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
   driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_saveExcelRadioButton\"]")).click();
   
   
       driver.findElement(By.xpath("//*[@id=\"ctl00_PCPortalMain_runReportButton\"]")).click();
}
   public void Help() throws InterruptedException
   {
	   Thread.sleep(3000);
	   
	   driver .findElement(By.xpath("/html/body/form/div[3]/div[2]/div/div/div[3]/ul/li[5]/a/span")).click();
	   Thread.sleep(2000);
	   driver.findElement(By.xpath("/html/body/form/div[3]/div[2]/div/div/div[5]/div[2]/div[2]/div/ul/li/a")).click();
   }
   
   
   
}