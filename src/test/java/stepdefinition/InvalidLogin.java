package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class InvalidLogin extends CucumberBase{
	
	@Given("^User is on Preparation Centre Portal URL$")
	public void user_is_on_Preparation_Centre_Portal_URL() throws Throwable {
		launchBrowser();
	  
	}

	@When("^invalid credentials are entered$")
	public void invalid_credentials_are_entered() throws Throwable {
		enterinvalidUserNamePassword();
	    	}

	@Then("^User should not be able to login$")
	public void user_should_not_be_able_to_login() throws Throwable {
	  
	}



}
