package stepdefinition;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Help extends CucumberBase {
	

@Given("^User will be navigate to the Preparation Centre Portal application page$")
public void user_will_be_navigate_to_the_Preparation_Centre_Portal_application_page() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	launchBrowser();
}

@When("^enters the correct credentials by user$")
public void enters_the_correct_credentials_by_user() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	enterUserNamePassword();
}

@Then("^User will be able to login to the application$")
public void user_will_be_able_to_login_to_the_application() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
}

@When("^User clicks on help menu$")
public void user_clicks_on_help_menu() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
	Help();
}

@Then("^Download FAQs file$")
public void download_FAQs_file() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    //throw new PendingException();
}



}
