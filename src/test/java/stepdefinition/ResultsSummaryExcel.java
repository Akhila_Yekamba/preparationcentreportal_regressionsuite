package stepdefinition;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ResultsSummaryExcel extends CucumberBase {
	@Given("^User will be able navigate to the Preparation Centre Portal application$")
	public void user_will_be_able_navigate_to_the_Preparation_Centre_Portal_application() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		launchBrowser();
	}

	@When("^User enters the correct credentials$")
	public void user_enters_the_correct_credentials() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		enterUserNamePassword();
	}

	@Then("^User will be able to login$")
	public void user_will_be_able_to_login() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("^User selects Exam name from the dropdown$")
	public void user_selects_Exam_name_from_the_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		prepCentreResultsView();
	}

	@When("^User selects session name from the dropdown$")
	public void user_selects_session_name_from_the_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("^clicks on  Go button$")
	public void clicks_on_Go_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}
	@Then("^user should be able to view the results$")
	public void user_should_be_able_to_view_the_results() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^user selects view as excel radio button$")
	public void user_selects_view_as_excel_radio_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		ResultSummaryExcelGenerate();
	}

	@When("^user clicks on the Run Report$")
	public void user_clicks_on_the_Run_Report() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@Then("^user should be able to view the result in downloaded excel file$")
	public void user_should_be_able_to_view_the_result_in_downloaded_excel_file() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}



}
