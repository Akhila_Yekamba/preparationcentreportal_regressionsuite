package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login extends CucumberBase {

	@Given("^User is on Preparation Centre Portal$")
	public void user_is_on_Preparation_Centre_Portal() throws Throwable {
		launchBrowser();
	}

	@When("^valid credentials are entered$")
	public void valid_credentials_are_entered() throws Throwable {
		enterUserNamePassword();
	    
	}

	@Then("^User should be able to login$")
	public void user_should_be_able_to_login() throws Throwable {
	   
	}	
}
