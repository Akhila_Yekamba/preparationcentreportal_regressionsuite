package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ViewResults extends CucumberBase{
	@Given("^User navigates to Preparation Centre Portal application$")
	public void user_navigates_to_Preparation_Centre_Portal_application() throws Throwable {
		launchBrowser();
	}

	@When("^User enters valid credentials$")
	public void user_enters_valid_credentials() throws Throwable {
		enterUserNamePassword();
	}

	@Then("^User should login$")
	public void user_should_login() throws Throwable {
	}

	

	@When("^User selects Exam from dropdown$")
	public void user_selects_Exam_from_dropdown() throws Throwable {
		prepCentreResultsView();
	}

	@When("^User selects Session from dropdown$")
	public void user_selects_Session_from_dropdown() throws Throwable {
	}

	@When("^clicks on Go$")
	public void clicks_on_Go() throws Throwable {
	}

	@Then("^user should be able to view the result$")
	public void user_should_be_able_to_view_the_result() throws Throwable {
	}

}
