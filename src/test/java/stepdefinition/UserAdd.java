
package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UserAdd extends CucumberBase
 {

@Given("^User is on prepcentre portal$")
public void user_is_on_prepcentre_portal() throws Throwable {
 launchBrowser();
 
}

@When("^user give valid details$")
public void user_give_valid_details() throws Throwable {
	enterUserNamePassword(); 
}

@Then("^user able to login$")
public void user_able_to_login() throws Throwable {
    
}

@When("^user click on My prfile and Additional User tab$")
public void user_click_on_My_prfile_and_Additional_User_tab() throws Throwable {
    UserAdd();
}

@Then("^user is on Additional user page$")
public void user_is_on_Additional_user_page() throws Throwable {
    
}

@When("^user enter valid details$")
public void user_enter_valid_details() throws Throwable {
}

@When("^click on Add additional user$")
public void click_on_Add_additional_user() throws Throwable {
    
}

@Then("^user should be able to add user$")
public void user_should_be_able_to_add_user() throws Throwable {
}}