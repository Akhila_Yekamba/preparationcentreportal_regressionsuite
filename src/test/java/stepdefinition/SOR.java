package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SOR extends CucumberBase
 {
 

@Given("^User is on Preparation Centre Portal application homepage$")
public void user_is_on_Preparation_Centre_Portal_application_homepage() throws Throwable {
  launchBrowser();  
}

@When("^User enter credentials$")
public void user_enter_credentials() throws Throwable {
	enterUserNamePassword();   
}

@Then("^User able to login$")
public void user_able_to_login() throws Throwable {
}

@When("^User choose Exam from dropdown$")
public void user_choose_Exam_from_dropdown() throws Throwable {
	prepCentreResultsView(); 
}

@When("^User choose  Session from dropdown$")
public void user_choose_Session_from_dropdown() throws Throwable {
    
}

@When("^click on Go button$")
public void click_on_Go_button() throws Throwable {
}

@Then("^user is able to view result page$")
public void user_is_able_to_view_result_page() throws Throwable {
    
}

@When("^user select View detailed candidate result as report type$")
public void user_select_View_detailed_candidate_result_as_report_type() throws Throwable {
	StatementofResult();   
}

@When("^user click on Run Report button$")
public void user_click_on_Run_Report_button() throws Throwable {
	
}

@Then("^user should be able to download SOR\\.$")
public void user_should_be_able_to_download_SOR() throws Throwable {
}
}

