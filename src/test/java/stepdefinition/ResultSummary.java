

package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ResultSummary extends CucumberBase{


@Given("^User navigate to Preparation Centre Portal application$")
public void user_navigate_to_Preparation_Centre_Portal_application() throws Throwable {
	launchBrowser();  
}

@When("^User enter valid credentials$")
public void user_enter_valid_credentials() throws Throwable {
	enterUserNamePassword();  
}

@Then("^User should able to login$")
public void user_should_able_to_login() throws Throwable {
    
}

@When("^User select Exam from dropdown$")
public void user_select_Exam_from_dropdown() throws Throwable {
	prepCentreResultsView();
}

@When("^User select Session from dropdown$")
public void user_select_Session_from_dropdown() throws Throwable {
}

@When("^click on Go$")
public void click_on_Go() throws Throwable {
}

@Then("^user is able to view result$")
public void user_is_able_to_view_result() throws Throwable {
    
}

@When("^user click on Run Report$")
public void user_click_on_Run_Report() throws Throwable {
	ResultSummary();  
}

@Then("^user should be able to view result summary page\\.$")
public void user_should_be_able_to_view_result_summary_page() throws Throwable {
}
}
