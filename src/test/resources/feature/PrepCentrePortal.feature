@prepCentrePortal
Feature: This feature file is to test login functionality of Preparation Centre Portal
@validLogin
 Scenario: To verify login with valid credentials
  Given User is on Preparation Centre Portal
  When valid credentials are entered 
  Then User should be able to login
  
@invalidLogin
 Scenario: To verify login with invalid credentials
 Given User is on Preparation Centre Portal URL
  When invalid credentials are entered 
 Then User should not be able to login"""
  
@results
  Scenario: To verify results functionality
  Given User navigates to Preparation Centre Portal application
	When User enters valid credentials
  Then User should login 
  When User selects Exam from dropdown
  And User selects Session from dropdown
  And clicks on Go
  Then user should be able to view the result  
  
   @StatmentofResult
 Scenario:Scenario: To verify SOR functionality
  Given User is on Preparation Centre Portal application homepage
	When User enter credentials
 Then User able to login 
 When User choose Exam from dropdown
  And User choose  Session from dropdown
  And click on Go button
  Then user is able to view result page 
  When user select View detailed candidate result as report type
  And user click on Run Report button
  Then user should be able to download SOR.
  
    
@ResultSummary
 
 Scenario: To verify Result Summary functionality
  Given User navigate to Preparation Centre Portal application
	When User enter valid credentials
  Then User should able to login 
  When User select Exam from dropdown
  And User select Session from dropdown
 And click on Go
  Then user is able to view result 
  When user click on Run Report
  Then user should be able to view result summary page.
  
  @UserAdd
  Scenario: To verify user add functionality
  Given User is on prepcentre portal
  When user give valid details
  Then user able to login
  When user click on My prfile and Additional User tab
  Then user is on Additional user page
  When user enter valid details
  And lick on Add additional user
  Then user should be able to add user 
  
  @ResultSummaryPDFGenerate
 
 Scenario: To verify Result Summary functionality
  Given User will navigate to Preparation Centre Portal application
	When User enter the valid credentials
  Then User should be login 
  When User select Exam from the dropdown
  And User select session from the dropdown
  And click on the Go button
  Then user is able to view the result 
  When user selects view as pdf radio button
  And user clicks on Run Report
  Then user should be able to view the result in downloaded pdf file
  
  @ResultSummaryExcelGenerate
 
Scenario: To verify Results Summary functionality
 Given User will be able navigate to the Preparation Centre Portal application
When User enters the correct credentials
Then User will be able to login 
 When User selects Exam name from the dropdown
And User selects session name from the dropdown
 And clicks on  Go button
Then user should be able to view the results 
  When user selects view as excel radio button
  And user clicks on the Run Report
  Then user should be able to view the result in downloaded excel file
  
  @Help
  Scenario: To verify Help functionality
  Given User will be navigate to the Preparation Centre Portal application page
	When  enters the correct credentials by user
 Then User will be able to login to the application
 When User clicks on help menu
  Then Download FAQs file"""
  
  
  
  
  
  
  